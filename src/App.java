import java.util.ArrayList;
public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Country> countries = new ArrayList<>();
		Country country1 = new Country("VN", "Viet Nam");
		countries.add(country1);
		countries.add(new Country("US", "USA"));
		countries.add(new Country("AU", "Australia"));
        System.out.println(countries);

        //add region cho Vietnam
        country1.addRegion(new Region("HN", "Ha Noi"));
		country1.addRegion(new Region("HCM", "Ho Chi Minh"));

		for (Country country : countries) {
			if (country.getCountryName().equals("Viet Nam")) {
				System.out.println("Regions of " + country.getCountryName() + ":");
				for (Region region : country.getRegions()) {
					System.out.println(region.getRegionName());
				}
			}
		}
	}

    
}
